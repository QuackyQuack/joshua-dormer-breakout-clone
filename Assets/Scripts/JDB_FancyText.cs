using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class JDB_FancyText : MonoBehaviour
{
    [Tooltip("Required")]
    [SerializeField] TextMeshProUGUI textMesh;

    [Tooltip("gameplay")]
    [SerializeField] Color flashCoulor;
    [SerializeField] float sizeIncrease;

    private Vector3 start_scale;
    private Color start_colour;

    IEnumerator scalingCorourine;

    private void Awake()
    {
        start_scale = transform.localScale;
        start_colour = textMesh.color;
    }

    /// <summary>
    /// Sets the text of this object
    /// </summary>
    /// <param name="_string">String to set to </param>
    /// <param name="animate">Do pop and flash animation</param>
    public void addText(string _string, bool animate = true)
    {
        textMesh.text = _string;

        if (animate)
        {
            if (scalingCorourine != null)
                StopCoroutine(scalingCorourine);

            scalingCorourine = scaleAnimate();

            StartCoroutine(scalingCorourine);
        }
    }



    IEnumerator scaleAnimate()
    {
        const float animationTime = 0.50f;
        float startTime = Time.time;

        Vector3 largestSize = start_scale * sizeIncrease;

        const float largestPercent = 0.65f;

        const float colorPercent = 0.1f; //Percent of time, with largest being the middle, that the text is the alternate colour

        float largest = animationTime * largestPercent;

        while ((Time.time - startTime ) < animationTime)
        {
            float done = (Time.time - startTime ) / animationTime;

            ///Calculate value of 0-1 for both the increasing and decreasing of the text size
            if (done > largest)
            {
                float remaining = 1 - largest;
                done = (1 - done)/remaining;
            }
            else
            {
                done /= largest;
            }

            textMesh.gameObject.transform.localScale = Vector3.Lerp(start_scale, largestSize, done);

            if (done > (1 - colorPercent))
            {
                textMesh.color = Color.Lerp(start_colour, flashCoulor,( (1 - done)/ colorPercent));
            }

            yield return null;
        }
        textMesh.gameObject.transform.localScale = start_scale;
        textMesh.color = start_colour;
    }


}
