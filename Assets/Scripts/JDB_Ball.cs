using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Mirror;
using Mirror.Experimental;

public class JDB_Ball : NetworkBehaviour
{
    [Header("Required")]
    [SerializeField] Rigidbody myRB;
    [SerializeField] NetworkLerpRigidbody netRigidBody;
    [SerializeField] GameObject myTrailRenderer;

    [Header("Gameplay")]
    [SerializeField] float minimumSpeed = 0;
    [SerializeField] float speedDecayPerBounce = 0;

    //Fixes a bug where overlapping players cause the ball to get stuck
    bool calculatedHitThisFrame = false;
    JDB_Player myPlayer = null;

    float collisionStartTime = 0.0f;

    //Used to store the velocity of the ball so that it can be used for calculations after events
    Vector3 storeVelocity = Vector3.zero;

    #region setup

    /// <summary>
    /// Set refrence to my player
    /// </summary>
    public void initialise(JDB_Player _Player)
    {
        myPlayer = _Player;
    }

    #endregion

    #region unity messages
    [ServerCallback]
    private void FixedUpdate()
    {
        myRB.velocity = storeVelocity;
        calculatedHitThisFrame = false;
    }


    /// <summary>
    /// Get normal of the other collider and bounce at angle 
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if (isServer)
        {
            collisionStartTime = Time.time;

            if (collision.gameObject.tag == "Player" && calculatedHitThisFrame)
                return;

            calculatedHitThisFrame = true;
            Vector3 curDirection = storeVelocity;
            Vector3 colliderNormal = collision.contacts[0].normal;

            //Reflect vector
            Vector3 newDir = curDirection - (2 * (Vector3.Dot(curDirection, colliderNormal)) * colliderNormal);


            //If the ball collided with a rigidbody, get the speed of that rigidbody and add it as a component of my new speed
            JDB_Player playerComponent = collision.gameObject.GetComponent< JDB_Player>();
            if (playerComponent)
            {
                newDir += Vector3.right * playerComponent.HorizontalVelocity;
            }



            //     Debug.DrawRay(collision.contacts[0].point, collision.contacts[0].normal * 100, Color.blue, 3);
            ////     Debug.DrawRay(collision.contacts[0].point, newDir * 100, Color.yellow, 3);

            //Could do use otherRB speed better
            storeVelocity = newDir.normalized * minimumSpeed;

            //let the ball of control on what it hits
            if (collision.gameObject.tag == "Block")
            {
                collision.gameObject.GetComponent<JDB_block>().ballCollided();
            }
        }

        if (isClient)
        {
            switch (collision.gameObject.tag)
            {
                case "Player": audioManager.playSound("paddle"); break;
                case "Block": audioManager.playSound("break"); break;

                default: audioManager.playSound("ballBounce"); break;
            }

        }
    }


        /// <summary>
        /// We are probably stuck on something, quickly launch in random direction
        /// </summary>
        /// <param name="collision"></param>
        [ServerCallback]
        private void OnCollisionStay(Collision collision)
        {
            if (Time.time - collisionStartTime > 0.4f)
            {
                storeVelocity = new Vector3(Random.Range(-1,1), Random.Range(-1, 1), Random.Range(-1, 1)).normalized * minimumSpeed;
            }
        }

        /// <summary>
        /// I hit the bottom of the screen, I need to respawn on my player
        /// </summary>
        /// <param name="collision"></param>
        //[ServerCallback]
        private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Bottom")
            return;

        if (isServer)
        {
            storeVelocity = Vector3.zero;
            myPlayer.respawnBall();
            myRB.isKinematic = true;
            myRB.position = transform.position; //Testing to see if this fixes wierd physics interaction with re-launching the ball
        }
        if (isClient)
        {
            audioManager.playSound("fail");
        }
    }

    #endregion

    #region gameplay

    /// <summary>
    /// randomise a direction and launch ball
    /// </summary>
    public void launchBall(Vector3 launchVelocity)
    {
        if (!isServer)
            return;
        gameObject.transform.parent = null;
        myRB.isKinematic = false;

        storeVelocity = launchVelocity;
    }

    /// <summary>
    /// Tell this ball to stop synchronising, dont need it while child of local player
    /// todo to server
    /// </summary>
    [Client]
    public void setBallPositionSync(bool _syncBall)
    {
        netRigidBody.enabled = _syncBall;
        myTrailRenderer.SetActive(_syncBall);
    }
    #endregion
}
