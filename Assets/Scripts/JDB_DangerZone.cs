using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class JDB_DangerZone : MonoBehaviour
{
    private static JDB_DangerZone instance;
    [SerializeField] Image mySPR;

    List<GameObject> ballList = new List<GameObject>();


    Color myCol;
    private void Awake()
    {
        instance = this;
        myCol = mySPR.color;
        myCol.a = 0.14f;
        mySPR.color = myCol;
    }

    /// <summary>
    /// Add new ball to track
    /// </summary>
    public static void addBall(GameObject _obj)
    {
        instance.ballList.Add(_obj);
    }
    /// <summary>
    /// Remove ball once the player has disconected
    /// </summary>
    public static void removeBall(GameObject _obj)
    {
        if(instance.ballList.Contains(_obj))
            instance.ballList.Remove(_obj);
    }

    private void Update()
    {
        if (ballList.Count == 0)
            return;

        ballList = ballList.OrderBy((x) => Mathf.Abs(transform.position.y - x.transform.position.y)).ToList();

        //Finds the lowest ball and uses it to flash the bottom area
        myCol.a = Mathf.Lerp(1, 0.14f, Mathf.Abs(transform.position.y - ballList[0].transform.position.y));
        mySPR.color = myCol;
    }

}
