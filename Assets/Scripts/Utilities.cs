using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utilities 
{
    public delegate void genericDelegate();

    public static void eventAfterXSeconds(float _xSeconds, genericDelegate _event)
    {
        gameManager.instance.StartCoroutine(waitSeconds(_xSeconds, _event));
    }

    //Waits x seconds then executes command
    static IEnumerator waitSeconds(float _xSeconds, genericDelegate _event)
    {
        yield return new WaitForSeconds(_xSeconds);
        _event();
    }

}
