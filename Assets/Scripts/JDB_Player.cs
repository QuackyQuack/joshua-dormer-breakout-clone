using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Mirror;


public class JDB_Player : NetworkBehaviour
{
    [Header("Required")]
    [SerializeField] Rigidbody myRB;
    [SerializeField] Transform ballSpawnPoint;
    [SerializeField] ParticleSystem ballPoof;

    [Header("Gameplay")]
    [SerializeField] float baseMoveSpeed = 0;
    [SerializeField] float ballLaunchForce = 0;


    JDB_Ball myBall = null;
    float horizontalVelocity = 0;
    public float HorizontalVelocity { get { return horizontalVelocity; } }

    #region unity messages

    [ClientCallback]
    void FixedUpdate()
    {
        if (!isLocalPlayer)
            return;
        float horizontalSpd = Input.GetAxisRaw("Horizontal") * baseMoveSpeed * Time.fixedDeltaTime;
        myRB.velocity = new Vector3(horizontalSpd, 0, 0);
        sendClientSpeed(horizontalSpd);
    }

    /// <summary>
    /// Update the velocity of player for the server
    /// </summary>
    /// <param name="_horizontal"></param>
    [Command]
    private void sendClientSpeed(float _horizontal)
    {
        horizontalVelocity = _horizontal;
    }

    [ClientCallback]
    private void Update()
    {
        if (!isLocalPlayer)
            return;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Tell the server to launch the ball in a random direction
            CMD_LaunchBall();
        }
    }

    #endregion

    #region setups

    public override void OnStartClient()
    {
        myRB.isKinematic = false;
        initialiseBall();
    }

    public override void OnStartServer()
    {
        myBall = Instantiate(JDB_NetworkManager.Instance.getSpawnPrefabs().Find(prefab => prefab.name == "Ball"), ballSpawnPoint).GetComponent<JDB_Ball>();
        NetworkServer.Spawn(myBall.gameObject, this.gameObject);
        myBall.initialise(this);
    }

    /// <summary>
    /// Server initialises the clients ball 
    /// </summary>
    [Command]
    public void initialiseBall()
    {
        setMyBall(myBall, hasBall);
    }

    /// <summary>
    /// Client sets ball, for me and other clients
    /// </summary>
    /// <param name="_myBall"></param>
    /// <param name="_attachBall"></param>
    [ClientRpc]
    private void setMyBall(JDB_Ball _myBall, bool _attachBall)
    {
        myBall = _myBall;
        JDB_DangerZone.addBall(myBall.gameObject);
        if(_attachBall)
            RPC_attachBall();
    }

    #endregion


    #region gameplay
    //Does this player currently have the ba;;
    bool hasBall { get { return myBall.transform.parent !=null; } }

    /// <summary>
    /// Client requests the server launches the ball
    /// </summary>
    [Command]
    private void CMD_LaunchBall()
    {
        if (!hasBall)
            return; //Catch any desync issues with the initial client side check
        RPC_dettachBall();
        float randomAngle = Random.Range(-45, 45);
        Vector3 launchDirection = Quaternion.Euler(0, 0, randomAngle) * Vector3.up;

        myBall.launchBall(launchDirection * ballLaunchForce);
    }


    /// <summary>
    /// Sets the ball back into the spawn position on this player
    /// </summary>
    [ServerCallback]
    public void respawnBall()
    {
        RPC_clientRespawnBall();
        myBall.transform.parent = ballSpawnPoint;
        myBall.transform.localPosition = Vector3.zero;
    }

    /// <summary>
    /// Handles client logic for respawning the ball
    /// </summary>
    [ClientRpc]
    public void RPC_clientRespawnBall()
    {
        ballPoof.Play();
        RPC_attachBall();
    }

    /// <summary>
    /// Attaches ball to point
    /// </summary>
    public void RPC_attachBall()
    {
        myBall.setBallPositionSync(false);
        myBall.transform.parent = ballSpawnPoint;
        myBall.transform.localPosition = Vector3.zero;
    }

    /// <summary>
    /// Detaches ball from point
    /// </summary>
    [ClientRpc]
    public void RPC_dettachBall()
    {
        myBall.setBallPositionSync(true);
        myBall.transform.parent = null;
    }

    public override void OnStopClient()
    {
        if(isLocalPlayer)
            gameManager.instance.waitForConnection();
        JDB_DangerZone.removeBall(myBall.gameObject);
    }

    #endregion
}
