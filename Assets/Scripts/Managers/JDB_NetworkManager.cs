using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Mirror;

public class JDB_NetworkManager : NetworkManager
{
    public static JDB_NetworkManager Instance;

    [SerializeField] GameObject[] spawns = new GameObject[0];
    [SerializeField] GameObject playArea ;
    public static GameObject  PlayArea { get { return Instance.playArea; } }

    List<JDB_Player> playerList = new List<JDB_Player>();

    public override void Awake()
    {
        base.Awake();
        Instance = this;
    }

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        //Create player and assign a round robin style spawn
        Vector3 mySpawnLocation = spawns[(numPlayers) % spawns.Length].transform.position;
        GameObject newPlayer = Instantiate(playerPrefab, mySpawnLocation, Quaternion.identity);
        NetworkServer.AddPlayerForConnection(conn, newPlayer);

        gameManager.instance.serverSendData();

        for (int x = 0; x < playerList.Count; x++)
        {
            playerList[x].initialiseBall();
        }

        playerList.Add(newPlayer.GetComponent<JDB_Player>());
    }

    public List<GameObject> getSpawnPrefabs()
    {
        return spawnPrefabs;
    }

}
