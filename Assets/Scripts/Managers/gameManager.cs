using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Mirror;
using TMPro;

public class gameManager : NetworkBehaviour
{
    #region required

    [SerializeField] GameObject blockPrefab;
    [SerializeField] GameObject blockParent;

    [SerializeField] JDB_FancyText scoreText;
    [SerializeField] TextMeshProUGUI backText;

    #endregion

    int totalScore = 0;


    Dictionary<int, JDB_block> blockContainer = new Dictionary<int, JDB_block>();
    List<int> inactiveBlocks = new List<int>();


    Gradient colourGradient;
    GradientColorKey[] colorKey;
    GradientAlphaKey[] alphaKey;


    //handles the noise pitching
    const float maxFrenzyValue = 5.0f;
    const float frenzyDecay = 1.2f;
    const float frenzyPerBlock = 1.0f;
    float myCurrentFrenzy = 0.0f;

    //Level generations
    const int lines = 9;
    const int blocksPerLine = 15;

    #region singleton vars/getters

    public static gameManager instance;


    #endregion

    #region Unity messages
    private void Awake()
    {
        if (instance)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;

        //Setup for the block colours

        colourGradient = new Gradient();

        colorKey = new GradientColorKey[3];
        colorKey[0].color = Color.red;
        colorKey[0].time = 0.0f;
        colorKey[1].color = Color.blue;
        colorKey[1].time = 0.5f;
        colorKey[2].color = Color.green;
        colorKey[2].time = 1.0f;

        alphaKey = new GradientAlphaKey[2];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;


        colourGradient.SetKeys(colorKey, alphaKey);

        //Pool all of the available blocks
        const float blockWidth = 1.1045f;
        const float blockHieght = 0.440625f;

        const float blockWidthWiggleRoom = 0.1f;
        const float blockHieghtWiggleRoom = 0.1f;

        float totalWidth = blocksPerLine * (blockWidth + (blockWidthWiggleRoom));
        float startXPos = blockParent.transform.position.x - (totalWidth * 0.5f);
        float startYPos = blockParent.transform.position.y;
        float zPos = blockParent.transform.position.z;

        int count = 0;
        for (int x = 0; x < lines; x++)
        {
            for (int y = 0; y < blocksPerLine; y++)
            {
                GameObject newBlock = Instantiate<GameObject>(blockPrefab, new Vector3(startXPos + (y * (blockWidth + (blockWidthWiggleRoom))) + (blockWidth*0.5f), startYPos - (x * (blockHieght + (blockHieghtWiggleRoom))), zPos), Quaternion.identity);
                newBlock.transform.parent = blockParent.transform;
                JDB_block blockComponent = newBlock.GetComponent<JDB_block>();
                blockComponent.initialise(count);
                blockContainer.Add(count, blockComponent);
                count++;

                //Colours

                float val = (float)x / ((float)lines-1);
                blockComponent.setMyColour(colourGradient.Evaluate(val));
            }
        }

        waitForConnection();
    }

    [ClientCallback]
    private void Update()
    {
        myCurrentFrenzy -= frenzyDecay * Time.deltaTime;
        myCurrentFrenzy = Mathf.Clamp(myCurrentFrenzy, 0, maxFrenzyValue);
    }


    #endregion

    #region setup

    public override void OnStartServer()
    {
        generateRandomLevel();
    }

    /// <summary>
    /// Removes random rows and columns in the pooled layout
    /// </summary>
    [ServerCallback]
    void generateRandomLevel()
    {
        resetLevel();
        totalScore = 0;
        instance.RPC_ShowWinnerText(false);
        //Takes out random columns and 

        //Remove no more than 3
        int rowsToRemve = Random.Range(0, 4);

        List<int> rowsRemaining = new List<int>();
        for (int x = 0; x < lines; x++)
        {
            rowsRemaining.Add(x);
        }

        for (int x = 0; x < rowsToRemve; x++)
        {
            int chosenRow = rowsRemaining[Random.Range(0, rowsRemaining.Count)];
            rowsRemaining.Remove(chosenRow);
            removeRow(chosenRow);
        }

        //Always make the rows symettrical so we dont do more than 4
        int lineToRemove = Random.Range(0, 3);

        List<int> coloumnsRemaining = new List<int>();

        for (int x = 0; x < blocksPerLine; x++)
        {
            coloumnsRemaining.Add(x);
        }

        for (int x = 0; x < lineToRemove; x++)
        {
            int chosenColumn = coloumnsRemaining[Random.Range(0, coloumnsRemaining.Count)];
            coloumnsRemaining.Remove(chosenColumn);
            removeColumn(chosenColumn);

            int val = (blocksPerLine - 1) - chosenColumn;
            coloumnsRemaining.Remove(val);
            removeColumn(val);

        }

        //Synchronise clients with new layout
        serverSendData();
    }

    private void removeRow(int rowNumber)
    {
        for (int x = 0; x < blocksPerLine; x++)
        {
            removeBlock((rowNumber * blocksPerLine) + x);
        }
    }

    private void removeColumn(int rowNumber)
    {
        for (int x = 0; x < lines; x++)
        {
            removeBlock((x * blocksPerLine) + rowNumber);
        }
    }


    /// <summary>
    /// Resets the level
    /// </summary>
    private void resetLevel()
    {
        if (isServer)
            inactiveBlocks.Clear();

        reloadBlocks();
    }

    /// <summary>
    /// Show all blocks again. Used by server and clients
    /// </summary>
    private void reloadBlocks()
    {
        for (int x = 0; x < blockContainer.Count; x++)
        {
            blockContainer[x].showBlock();
        }
    }

    /// <summary>
    /// Hide all blocks and display the waiting for connection message
    /// </summary>
    public void waitForConnection()
    {
        scoreText.addText("WAITING FOR CONNECTION", false);

        for (int x = 0; x < blockContainer.Count; x++)
        {
            blockContainer[x].hideBlock();
        }
    }


    /// <summary>
    /// Sync client with the server
    /// </summary>
    [Server]
    public void serverSendData()
    {
        RPC_clientSyc(inactiveBlocks.ToArray(), totalScore);
    }

    /// <summary>
    /// Client gets latests room infor and syncs layout
    /// </summary>
    [ClientRpc]
    public void RPC_clientSyc(int[] _alreadyBrokeBlocks, int _currentScore)
    {
        reloadBlocks();
        scoreText.addText("SCORE " + _currentScore.ToString(), false);

        for (int x = 0; x < _alreadyBrokeBlocks.Length; x++)
        {
            JDB_block blockComponent = instance.blockContainer[_alreadyBrokeBlocks[x]];
            blockComponent.hideBlock();
        }
    }

    #endregion

    #region gameplay

    /// <summary>
    /// Removes block from the current level
    /// </summary>
    /// <param name="_blockID"></param>
    [ServerCallback]
    private void removeBlock(int _blockID)
    {
        if (!instance.inactiveBlocks.Contains(_blockID))
            instance.inactiveBlocks.Add(_blockID);
        blockContainer[_blockID].hideBlock();
    }

    [ServerCallback]
    public void blockHit(int _block)
    {
        instance.totalScore += JDB_block.blockPoints;
        instance.inactiveBlocks.Add(_block);
        instance.RPC_blockBroken(_block, instance.totalScore);

        //Check if all blocks are gone
        if (instance.inactiveBlocks.Count == instance.blockContainer.Count)
        {
            //WINNER WINNER CHICKEN DINNER
            instance.RPC_ShowWinnerText(true);

            //Wait for a few seconds and then play a new level
            Utilities.eventAfterXSeconds(5.0f, () => { generateRandomLevel(); });
        }
    }


    [ClientRpc]
    public void RPC_ShowWinnerText(bool show)
    {
        backText.gameObject.SetActive(show);
        if (show)
            audioManager.playSound("chickenDinner");
    }


    [ClientRpc]
    public void RPC_blockBroken(int _block, int _newScore)
    {
        scoreText.addText("SCORE " + _newScore.ToString());
        JDB_block blockComponent = instance.blockContainer[_block];
        blockComponent.blockBroken();
        myCurrentFrenzy += frenzyPerBlock;
        audioManager.playSound("frenzy", Mathf.Lerp(1.0f, 2.5f, myCurrentFrenzy / maxFrenzyValue) + Random.Range(-0.1f, 0.15f));
    }
    #endregion


}
