using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioManager : MonoBehaviour
{
    [System.Serializable]
    public class soundClass
    {
        public string soundName;
        public AudioClip soundFile;
        public float baseVolumn = 1.0f;

        [HideInInspector]
        public AudioSource mySource;
    }


    private static audioManager instance;

    [SerializeField] soundClass[] mySounds = new soundClass[0];

    private void Awake()
    {
        instance = this;

        for (int x = 0; x < mySounds.Length; x++)
        {
            mySounds[x].mySource = Camera.main.gameObject.AddComponent<AudioSource>();
            mySounds[x].mySource.clip = mySounds[x].soundFile;
            mySounds[x].mySource.volume = mySounds[x].baseVolumn;
            mySounds[x].mySource.playOnAwake = false;
        }

    }

    /// <summary>
    /// Plays 2D sound
    /// </summary>
    public static void playSound(string _soundName, float pitch = 1.0f)
    {
        for (int x = 0; x < instance.mySounds.Length; x++)
        {
            if (instance.mySounds[x].soundName.ToLower() == _soundName.ToLower())
            {
                instance.mySounds[x].mySource.Play();
                instance.mySounds[x].mySource.pitch = pitch;

                return;
            }
        }
    }

}
