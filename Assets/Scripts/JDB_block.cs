using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JDB_block : MonoBehaviour
{
    public const int blockPoints = 100;

    [SerializeField] GameObject myCollider;
    [SerializeField] GameObject myMesh;
    [SerializeField] ParticleSystem myParticles;

    Renderer myRenderer;

    private int myID;


    private void Awake()
    {
        myRenderer = myMesh.GetComponent<Renderer>();
    }

    /// <summary>
    /// Sync ID
    /// </summary>
    public void initialise(int _id)
    {
        myID = _id;
    }

    /// <summary>
    /// Server only
    /// </summary>
    public void ballCollided()
    {
        myCollider.gameObject.SetActive(false);
        myMesh.gameObject.SetActive(false);

        gameManager.instance.blockHit(myID);
    }

    /// <summary>
    /// Dissable the block without the breaking animation
    /// </summary>
    public void hideBlock()
    {
        myMesh.gameObject.SetActive(false);
        myCollider.gameObject.SetActive(false);
    }

    /// <summary>
    /// re-enables the block
    /// </summary>
    public void showBlock()
    {
        myMesh.gameObject.SetActive(true);
        myCollider.gameObject.SetActive(true);
    }

    /// <summary>
    /// Only called on clients, plays the animation
    /// </summary>
    public void blockBroken()
    {
        myMesh.gameObject.SetActive(false);
        myCollider.gameObject.SetActive(false);
        myParticles.Play();
    }

    /// <summary>
    /// Only called on client to set the colour of the block
    /// </summary>
    public void setMyColour(Color _newColour)
    {
        // Could do - Save material for all cubes which are the same colour or just write a shader for them
        Material instancedMaterial = myRenderer.material;
        instancedMaterial.color = _newColour;

        myParticles.GetComponent<ParticleSystemRenderer>().material = instancedMaterial;
    }
}
